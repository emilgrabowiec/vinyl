<?php

class vinyl_bootstrap {
	
	/**
	 *
	 * @var string
	 */
	protected $baseUrl;
	
	/**
	 * 
	 * @var array
	 */
	protected $request;
	
	/**
	 * 
	 * @var vinyl_view
	 */
	protected $view;
	
	public function __construct() {
		$this->request = array_merge($_POST, $_GET);
		
		/* prepare base name for controller and action from request */
		$controller = !isset($this->request['controller']) || is_null($this->request['controller']) ? 'index' : strtolower($this->request['controller']);
		$action 	= !isset($this->request['action']) || is_null($this->request['action']) ? 'index' : strtolower($this->request['action']);
		
		/* 
		 * prepare names for controller and action to invoke file, class and action
		 * */

		/* controller */
		$arrTmpController = explode('-', $controller);
		foreach ($arrTmpController as &$ctrlPart) {
			$ctrlPart = ucfirst($ctrlPart);
		}
		$invController = implode('', $arrTmpController);

		/* action */
		$arrTmpAction = explode('-', $action);
		foreach ($arrTmpAction as &$actPart) {
			$actPart = ucfirst($actPart);
		}
		$invAction = lcfirst(implode('', $arrTmpAction)) . 'Action';
		
		$nameController	= sprintf('%sController.php', $invController);
		$nameClass		= sprintf('%sController', $invController);
		require APPLICATION_PATH . '/controllers/' . $nameController;
		
		/* create class instance */
		$classInstance = new $nameClass();
		$classInstance->setRequest($this->request);
		$classInstance->$invAction();
		
		$this->view = $classInstance->getView();
		$this->view->setRequest($this->request);
		
		$this->view->render($controller, $action);
	}
}