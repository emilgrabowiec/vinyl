<?php

/**
 * class should init autoloader which autoloads files with classes placed in library/ directory
 */
class vinyl_autoloader {
	
// 	/**
// 	 * 
// 	 * @var array
// 	 */
// 	protected $namespaces;
	
// 	/**
// 	 * 
// 	 * @param array $namespaces array with namespaces which should be accessible to autoload files with classes placed in library/ directory
// 	 */
	public function __construct($namespaces = null) {
// 		$this->namespaces = array('library');
		
// 		if(is_array($namespaces) && !is_null($namespaces)) 
// 			$this->namespaces = array_merge($this->namespaces, $namespaces);
	}
	
	public function initAutoloader() {
		spl_autoload_register(function ($class) {
			$classPath = implode('/', explode('_', $class));
			include APPLICATION_PATH . '/../library/' . $classPath . '.php';
		});
	}
}