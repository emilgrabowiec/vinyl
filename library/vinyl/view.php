<?php

class vinyl_view {
	
	protected $request;
	protected $baseUrl;
	
	public function __construct() {
		$this->initBaseUrl();
	}
	
	/**
	 * Function returns value for specified field which exists in request array of view object
	 * @param string $field name of field
	 * @param string $default default values which should appear if field has no value
	 * @return string
	 */
	public function value($field, $default = '') {
		$val = isset($this->request[$field]) ?  $this->request[$field] : '';
	
		return $val == '' ? $default : $val;
	}
	
	/**
	 * Function initializes base url for applications depending on server url
	 */
	private function initBaseUrl() {
		$this->baseUrl = sprintf(
			"%s://%s%s",
			isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
			$_SERVER['HTTP_HOST'],
			APPLICATION_ROOT
		);
	}
	
	/**
	 * Function creates url based on provided controller and action
	 * @param array $arrData required elements: 'action', 'controller'
	 * @throws Exception if missing controller or action in arrData
	 * @return string 
	 */
	public function url($arrData) {
		if(!isset($arrData['controller'])) throw new Exception('Missing controller name for url.');
		if(!isset($arrData['action'])) throw new Exception('Missing action name for url.');
	
		return sprintf('%s/%s/%s',
			$this->baseUrl,
			$arrData['controller'],
			$arrData['action']
		);
	}
	
	public function setRequest($request) {
		$this->request = $request;
	}
	
	public function render($controller, $action) {
		require APPLICATION_PATH . sprintf('/views/layout/header.phtml');
		require APPLICATION_PATH . sprintf('/views/%s/%s.phtml', $controller, $action);
		require APPLICATION_PATH . sprintf('/views/layout/footer.phtml');
	}
	
	public function baseUrl($url = null) {
		return (is_null($url)) ? $this->baseUrl : $this->baseUrl . '/' . $url;
	}
}