<?php
/*
 * Add in this file any custom function which will be use in any place in the code.
 */


/**
 * Function prints formatted variable to the screen (e.g. array key => values with indentations)
 * 
 * @param mixed $var variable to be print
 */
function pre($var) {
	print_r('<pre>');
	print_r($var);
	print_r('</pre>');
}